import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-added-to-cart',
  templateUrl: './added-to-cart.component.html',
  styleUrls: ['./added-to-cart.component.css']
})
export class AddedToCartComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
}
