import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-cart-items',
  templateUrl: './no-cart-items.component.html',
  styleUrls: ['./no-cart-items.component.css']
})
export class NoCartItemsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
