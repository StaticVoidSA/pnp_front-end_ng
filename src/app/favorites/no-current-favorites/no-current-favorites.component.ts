import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-current-favorites',
  templateUrl: './no-current-favorites.component.html',
  styleUrls: ['./no-current-favorites.component.css']
})
export class NoCurrentFavoritesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
