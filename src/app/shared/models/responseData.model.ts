export interface RespData {
  continent: "",
  continentCode: "",
  countryName: "",
  latitude: null,
  longitude: null,
  locality: "",
  localityInfo: { administrative: null, informative: null },
  localityLanguageRequested: "",
  principalSubdivision: ""
}
