export interface ProductUpdate {
    productID: number;
    title: string;
    category: string;
    brand: string;
    uri: string;
    price: number;
    description: string;
    features: string;
    usage: string;
    quantity: string;
    barcode: string;
    _barcode: string;
}