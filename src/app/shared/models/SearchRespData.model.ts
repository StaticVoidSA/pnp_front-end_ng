export interface SearchRespData {
    productID: number;
    title: string;
    brand: string;
    category: string;
    price: number;
    uri: string;
    description: string;
    features: string;
    usage: string;
    quantity: string;
    barcode: string;
}
