import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { NguCarouselModule } from '@ngu/carousel';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
    imports: [
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatMenuModule,
        MatButtonModule,
        ShowHidePasswordModule,
        MatGridListModule,
        MatPaginatorModule,
        MatTableModule,
        MatSortModule,
        MatBadgeModule,
        MatInputModule,
        MatTooltipModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        MatDialogModule,
        MatSelectModule,
        NguCarouselModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatRadioModule,
        MatNativeDateModule
    ],
    exports: [
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatMenuModule,
        MatButtonModule,
        ShowHidePasswordModule,
        MatGridListModule,
        MatPaginatorModule,
        MatTableModule, 
        MatSortModule,
        MatBadgeModule,
        MatInputModule,
        MatTooltipModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        MatDialogModule,
        MatSelectModule,
        NguCarouselModule,
        MatExpansionModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatRadioModule,
        MatNativeDateModule
    ]
})

export class MaterialModule {}
